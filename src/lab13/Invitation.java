/**
 * @author Johnny Tsheke @ UQAM
 * 2017-04-11
 */
package lab13;

/**
 * @author johnny
 *
 */
public class Invitation extends FairePart {
    private static int compteur = 1;
    
    public Invitation(String nomInvite){
    	this.setNom(nomInvite);
    	this.setNumero(compteur);
    	compteur++;
    }
	@Override
	public void setNom(String n) {
		this.nom = n;
	}

	@Override
	public String getNom() {
		
		return this.nom;
	}

	@Override
	public void setNumero(int num) {
		this.numero = num;

	}
	@Override
	public int getNumero() {
		// TODO Auto-generated method stub
		return this.numero;
	}
    
}
