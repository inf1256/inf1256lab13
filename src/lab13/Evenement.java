package lab13;

import java.util.*;
import lab13.*;
public class Evenement {
	 Map<Integer,Invitation> invites = new HashMap<Integer,Invitation>(); 
     Scanner clavier;
	public static void main(String[] args) {
		Evenement ev = new Evenement();
		ev.clavier = new Scanner(System.in).useDelimiter(System.lineSeparator());
		//boucle menu
		String pattern="[1-3]";
		boolean stop = false;
	     while(!stop){
			System.out.println("Veuillez choisir un menu svp");
			System.out.println("1: AJouter une invitation");
			System.out.println("2: Afficher la liste des invités");
			System.out.println("3: Quitter");
			if(ev.clavier.hasNext(pattern)){
				int menu =ev.clavier.nextInt();
				switch(menu){
				case 1: {ev.ajoutInvitation();
					break;}
				case 2:{ev.afficheInvites();
					break;}
				case 3: stop=true;
				}
			}else{
				System.out.println("Choix incorrect. Veuilez fare un bon choix");
			}
				}
		ev.clavier.close();
	}
	
	void ajoutInvitation(){//on ne fait pas la validation
		System.out.println("Entrer le nom de l'invité svp");
		String nomInvite = this.clavier.next();
		Invitation inv = new Invitation(nomInvite);
		int num = inv.getNumero();
		this.invites.put(num,inv);
	}
	
	void afficheInvites(){
		for(int num:this.invites.keySet()){
			Invitation inv =this.invites.get(num);
			System.out.println("-----------");
			System.out.println("Numero invitation: "+ inv.getNumero());
			System.out.println("Nom invité: "+ inv.getNom());
			System.out.println("");
		}
	}
    
}
