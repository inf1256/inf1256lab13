/**
 * @author Johnny Tsheke @ UQAM
 * 2017-04-11
 */
package lab13;


public abstract class FairePart {
        protected String nom;
        protected int numero;
        public abstract void setNom(String n);
        public abstract String getNom();
        public abstract void setNumero(int num);
        public abstract int getNumero();
}
